# Serverless OCR API

This repository contains a serverless OCR API written in python 3.7 and deployed to AWS. The API has two endpoints which accept a base64 encoded image file or an url pointing to an image, respectively.

## Step 1: Include Tesseract binaries

```
./build.sh
```

## Step 2: Deploy to AWS

```
serverless deploy
```

## Step 3: Test endpoints

```
curl -X POST "$ENDPOINT"/dev/url -d https://i.stack.imgur.com/vrkIj.png
```

```
cat lorem_ipsum.png | base64 | curl -X POST "$ENDPOINT"/dev/file --data @-
```
